package main;

import admin.AdminServlet;
import auth.AccountServiceImpl;
import base.AccountService;
import base.DBService;
import database.DBServiceImpl;
import frontend.*;
import gameplay.GameCycleImpl;
import mobile.MobileServlet;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jetbrains.annotations.NotNull;
import resourceSystem.ResourceFactory;
import resourceSystem.resourceObjects.ServerConfig;

import javax.servlet.Servlet;
import javax.servlet.SessionCookieConfig;
import java.io.FileNotFoundException;
import java.sql.SQLException;

public class Main {

    @SuppressWarnings("OverlyBroadThrowsClause")
    public static void main(@NotNull String[] args) throws Exception {

        ResourceFactory.getInstance().init("data");

        int port;
        try {
            ServerConfig serverConfig = ServerConfig.getInstance();
            port = serverConfig.getPort();
        } catch (FileNotFoundException e) {
            System.out.println("Can't find server start config");
            return;
        } catch (NumberFormatException e) {
            System.out.println("Invalid port in cfg/server.properties");
            return;
        }

        DBService DataBase;
        try {
            DataBase = new DBServiceImpl();
        }
        catch (SQLException e)
        {
            System.out.println("Problems with DB");
            return;
        }
        AccountService UsersBase = new AccountServiceImpl(DataBase);
        GameCycleImpl gameCycle = new GameCycleImpl();

        Servlet signin = new SignInServlet(UsersBase);
        Servlet signup = new SignUpServlet(UsersBase);
        Servlet logout = new LogOut(UsersBase);
        Servlet profile = new ProfileServlet(UsersBase);
        Servlet admin = new AdminServlet(UsersBase, gameCycle);
        Servlet game = new GameServlet(UsersBase, DataBase);
        Servlet status = new StatServlet(DataBase);
        Servlet config = new ConfigServlet();
        Servlet mobile = new MobileServlet(UsersBase);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        context.addServlet(new ServletHolder(signin), "/api/v1/auth/signin/");
        context.addServlet(new ServletHolder(signup), "/api/v1/auth/signup/");
        context.addServlet(new ServletHolder(logout), "/api/v1/auth/logout/");
        context.addServlet(new ServletHolder(profile), "/api/v1/auth/profile/");
        context.addServlet(new ServletHolder(admin), "/api/v1/auth/admin/");
        context.addServlet(new ServletHolder(game), "/game/");
        context.addServlet(new ServletHolder(status), "/status/");
        context.addServlet(new ServletHolder(config), "/config/");
        context.addServlet(new ServletHolder(mobile), "/mobile/");

        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setDirectoriesListed(true);
        resource_handler.setResourceBase("public_html");
        resource_handler.setWelcomeFiles(new String[]{"index.html"});

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{resource_handler, context});

        Server server = new Server(port);
        server.setHandler(handlers);

        server.start();

        gameCycle.run();
    }
}
