package auth;

import base.AccountService;
import base.DBService;
import database.datasets.UserProfile;
import mobile.RandomString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import websockets.GameSocket;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by iHelos on 20.09.2015.
 */
public final class AccountServiceImpl implements AccountService {

    DBService dataBase;

    @NotNull
    private final Map<String, UserProfile> users = new ConcurrentHashMap<>();
    @NotNull
    private final Map<String, UserProfile> sessions = new ConcurrentHashMap<>();
    @NotNull
    private final Map<UserProfile, String> sessionsRevert = new ConcurrentHashMap<>();

    @NotNull
    private final Map<String, GameSocket> mobileSockets = new ConcurrentHashMap<>();

    @NotNull
    private final Map<String, GameSocket> sockets = new ConcurrentHashMap<>();

    public AccountServiceImpl(DBService dataBase) throws SQLException {
        this.dataBase = dataBase;
        ArrayList<UserProfile> usersDB = dataBase.getUsers();
        for (UserProfile user : usersDB) {
            this.users.put(user.getLogin(), user);
        }
    }

    @Override
    public boolean addUser(@NotNull String userName, @NotNull UserProfile userProfile) {
        if (users.containsKey(userName))
            return false;
        try {
            dataBase.addUser(userProfile);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        users.put(userName, userProfile);
        System.out.println(userName + " registered");
        return true;
    }

    @Override
    public void addSessions(@NotNull String sessionId, @NotNull UserProfile userProfile) {
        UserProfile profile = this.getSessions(sessionId);
        if (profile != null) {
            GameSocket socket = profile.getUserSocket();
            if (socket != null) {
                System.out.println("another login from same Browser");
                socket.getSession().close();
            }
        }
        String anotherSession = getSessionsRevert(userProfile);
        if (anotherSession != null) {
            System.out.println("another login from another PC");
            deleteSessions(anotherSession);
        }
        sessionsRevert.put(userProfile, sessionId);
        sessions.put(sessionId, userProfile);
        System.out.println(userProfile.getLogin() + " signed in");
    }

    @Override
    public boolean deleteSessions(@NotNull String sessionId) {
        UserProfile profile = getSessions(sessionId);
        if (profile != null) {
            sessions.remove(sessionId);
            sessionsRevert.remove(profile);
            GameSocket socket = profile.getUserSocket();
            if (socket != null) {
                socket.getSession().close();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Nullable
    public UserProfile getUser(String userName) {
        return users.get(userName);
    }

    @Override
    @Nullable
    public UserProfile getSessions(String sessionId) {
        if (sessions.containsKey(sessionId))
            return sessions.get(sessionId);
        else
            return null;
    }

    @Nullable
    private String getSessionsRevert(UserProfile player) {
        if (sessionsRevert.containsKey(player))
            return sessionsRevert.get(player);
        else
            return null;
    }

    @Override
    public int getRegisteredCount() {
        return users.size();
    }

    @Override
    public int getLoggedCount() {
        return sessions.size();
    }

    @Override
    public String addMobilePass(GameSocket game)
    {
        String pass = RandomString.getRandom();
        while (mobileSockets.containsKey(pass))
            pass = RandomString.getRandom();
        mobileSockets.put(pass, game);
        return pass;
    }

    @Override
    public void removeMobilePass(String pass)
    {
        if (mobileSockets.containsKey(pass))
            mobileSockets.remove(pass);
    }

    @Override
    @Nullable
    public GameSocket getMobile(String pass)
    {
        if (mobileSockets.containsKey(pass))
            return  mobileSockets.get(pass);
        else
            return null;
    }

    @Override
    public void addProfileSocket(String profile, GameSocket socket)
    {
        sockets.put(profile, socket);
    }

    @Override
    public void deleteProfileSocket(String profile)
    {
        if(sockets.containsKey(profile))
            sockets.remove(profile);
    }

    @Override
    public Boolean checkProfileSocket(String profile)
    {
        return sockets.containsKey(profile);
    }
}
