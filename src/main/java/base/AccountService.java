package base;

import database.datasets.UserProfile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import websockets.GameSocket;

/**
 * Created by iHelos on 20.09.2015.
 */
public interface AccountService {

    boolean addUser(@NotNull String userName, @NotNull UserProfile userProfile);

    void addSessions(@NotNull String sessionId, @NotNull UserProfile userProfile);

    boolean deleteSessions(@NotNull String sessionId);

    @Nullable
    UserProfile getUser(String userName);

    @Nullable
    UserProfile getSessions(String sessionId);

    int getRegisteredCount();

    int getLoggedCount();

    String addMobilePass(GameSocket game);

    void removeMobilePass(String pass);

    GameSocket getMobile(String pass);

    void addProfileSocket(String profile, GameSocket socket);

    void deleteProfileSocket(String profile);

    Boolean checkProfileSocket(String profile);
}
