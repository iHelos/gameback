package base;

import database.datasets.UserProfile;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by olegermakov on 20.11.15.
 */
public interface DBService {

    ArrayList<UserProfile> getUsers() throws SQLException;

    ArrayList<UserProfile> getTen() throws SQLException;

    boolean addUser(UserProfile user) throws SQLException;

    void shutdown() throws SQLException;

    void setWin(UserProfile user) throws SQLException;
    void setDraw(UserProfile user) throws SQLException;
    void setLose(UserProfile user) throws SQLException;
}
