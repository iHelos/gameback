package base;

import com.google.gson.JsonObject;
import gameplay.GamePlayer;
import gameplay.GameSession;
import org.jetbrains.annotations.NotNull;
import websockets.GameSocket;

import java.util.Map;

/**
 * Created by iHelo on 22.10.2015.
 */
public interface WebsocketService {

    void notifyGameOver(GamePlayer user, boolean win);

    void sendMenuUsers(JsonObject data);

    @NotNull
    Boolean lobbyExists(String name);

    @NotNull
    GameSession createLobby(String name, GameSocket creator, DBService DB, int members);

    @NotNull
    Map<String, GameSession> getOpenLobbies();

    void addMenuUser(GameSocket user);

    void removeMenuUser(GameSocket user);

    void removeGamer(GameSocket user);

    boolean userToLobby(GameSocket user, GameSession session);
}
