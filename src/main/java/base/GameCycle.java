
package base;

/**
 * Created by iHelo on 22.10.2015.
 */


@SuppressWarnings("unused")
public interface GameCycle {
    void run();

    void sessionStep();
    void stop();
}
