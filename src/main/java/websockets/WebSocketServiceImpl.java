package websockets;

import base.DBService;
import base.WebsocketService;
import com.google.gson.JsonObject;
import database.DBServiceImpl;
import gameplay.GamePlayer;
import gameplay.GameSession;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by iHelo on 22.10.2015.
 */
public final class WebSocketServiceImpl implements WebsocketService {
    private Map<String, GameSession> openLobbies;

    @SuppressWarnings("FieldCanBeLocal")
    @NotNull
    private Set<GameSocket> usersInMenu;

    private static int countTeams;
    private static int countMembers;

    private static Set<GameSocket> gamers;

    private static WebSocketServiceImpl instance;

    private WebSocketServiceImpl() {
        openLobbies = new ConcurrentHashMap<>();
        usersInMenu = Collections.newSetFromMap(new ConcurrentHashMap<>());
        gamers = Collections.newSetFromMap(new ConcurrentHashMap<>());
    }

    public static WebSocketServiceImpl getInstance() {
        if (instance == null)
            instance = new WebSocketServiceImpl();
        return instance;
    }

    @Override
    public void notifyGameOver(GamePlayer user, boolean win) {

    }

    @Override
    public void sendMenuUsers(JsonObject data) {
        for (GameSocket user : usersInMenu) {
            try {
                user.getSession().getRemote().sendString(data.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    @Override
    public Boolean lobbyExists(String name) {
        return openLobbies.containsKey(name);
    }

    public void removeLobby(GameSession lobby) {
        openLobbies.remove(lobby.getName());
    }

    @NotNull
    @Override
    public GameSession createLobby(String name, GameSocket creator, DBService DB, int members) {
        removeMenuUser(creator);
        GameSession Lobby = new GameSession(name, countTeams, members, DB);
        Lobby.addUser(creator);
        openLobbies.put(Lobby.getName(), Lobby);
        return Lobby;
    }

    @NotNull
    @Override
    public Map<String, GameSession> getOpenLobbies() {
        return openLobbies;
    }

    @Override
    public void addMenuUser(GameSocket user) {
        usersInMenu.add(user);
    }

    @Override
    public void removeMenuUser(GameSocket user) {
        usersInMenu.remove(user);
    }

    @Override
    public void removeGamer(GameSocket user) {
        gamers.remove(user);
    }

    @Override
    public boolean userToLobby(GameSocket user, GameSession session)
    {
        if(!gamers.contains(user)) {
            usersInMenu.remove(user);
            gamers.add(user);
            session.addUser(user);
            if (session.getUserCount() == session.getMaxCount())
                session.autoGameStart();
            return true;
        }
        else {
            return false;
        }
    }

    public static void setProperties(int teams, int members) {
        countTeams = teams;
        countMembers = members;
    }
}
