package websockets;

import base.AccountService;
import base.DBService;
import database.DBServiceImpl;
import database.datasets.UserProfile;
import gameplay.GamePlayer;
import mobile.RandomString;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * Created by iHelo on 22.10.2015.
 */
public class GameSocketCreator implements WebSocketCreator {
    private AccountService authService;
    private DBService DB;

    public GameSocketCreator(AccountService base, DBService DB) {
        authService = base;
        this.DB = DB;
    }

    @Nullable
    @Override
    public Object createWebSocket(ServletUpgradeRequest servletUpgradeRequest, ServletUpgradeResponse servletUpgradeResponse) {
        String sessionId = servletUpgradeRequest.getHttpServletRequest().getSession().getId();

        UserProfile usr = authService.getSessions(sessionId);
        if (usr != null) {
            if(authService.checkProfileSocket(usr.getLogin()))
                return null;
            GameSocket socket = new GameSocket(usr, DB, authService);
            String password = authService.addMobilePass(socket);
            socket.setMobilePass(password);
            authService.addProfileSocket(usr.getLogin(),socket);
            usr.setUserSocket(socket);
            return socket;
        } else {
            return null;
        }
    }
}
