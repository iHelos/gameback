package websockets;

import base.AccountService;
import base.DBService;
import base.WebsocketService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import database.datasets.UserProfile;
import gameplay.GamePlayer;
import gameplay.GameSession;
import mobile.RandomString;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Map;

@WebSocket
public class GameSocket {
    private GamePlayer user;
    private Session session;
    private GameSession lobby;
    @NotNull
    private WebsocketService GameService;

    private DBService DB;
    private AccountService auth;

    String mobilePassword;

    public enum ReceiveCodes {
        NOPE,
        CREATELOBBY,
        USERCAME,
        START,
        MOVERIGHT,
        MOVELEFT,
        MOVEUP,
        MOVEDOWN,
        EXITLOBBY,
        REFRESHLOBBYLIST
    }

    public enum SendCodes {
        INIT,
        INFONEWLOBBY,
        CREATESUCCESS,
        CREATEFAILED,
        CAMESUCCESS,
        CAMEFAILED,
        NEWUSERMENU,
        NEWUSERLOBBY,
        STARTINFO,
        NOTSTARTED,
        WORLDINFO,
        GAMEOVER,
        GOAL,
        USERLEFTLOBBY
    }

    private ReceiveCodes getCode(int i) {
        return ReceiveCodes.values()[i];
    }

    public GameSocket(UserProfile User, DBService DB, AccountService auth) {
        this.DB = DB;
        this.auth = auth;
        GameService = WebSocketServiceImpl.getInstance();
        user = new GamePlayer(User);
    }

    @SuppressWarnings("OverlyComplexMethod")
    @OnWebSocketMessage
    public void onMessage(String data) {
        //noinspection TryWithIdenticalCatches
        try {
            JsonObject msg = new Gson().fromJson(data, JsonObject.class);
            ReceiveCodes UserCode = getCode(msg.get("code").getAsInt());
            switch (UserCode) {
                case NOPE:
                    break;
                case CREATELOBBY:
                    session.getRemote().sendString(userCreatingLobby(msg).toString());
                    sendLobbiesList();
                    break;
                case USERCAME:
                    session.getRemote().sendString(userComingLobby(msg).toString());
                    sendLobbiesList();
                    break;
                case START:
                    gameStart();
                    sendLobbiesList();
                    break;
                case MOVERIGHT:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move right;");
                        user.rightVx();
                    }
                    break;
                case MOVELEFT:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move left;");
                        user.leftVx();
                    }
                    break;
                case MOVEUP:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move up;");
                        user.topVy();
                    }
                    break;
                case MOVEDOWN:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move down;");
                        user.bottomVy();
                    }
                    break;
                case EXITLOBBY: {
                    lobby.removeUser(this);
                    GameService.removeGamer(this);
                    this.lobby = null;
                    GameService.addMenuUser(this);
                    sendLobbiesList();
                }
                break;
                case REFRESHLOBBYLIST:
                    JsonObject Msg = new JsonObject();
                    Msg.addProperty("code", SendCodes.INIT.ordinal());
                    Msg.add("lobbies", lobbiesList());
                    session.getRemote().sendString(Msg.toString());
                    break;
                default:
                    break;

            }
        } catch (JsonSyntaxException e) {
            System.out.println("error with msg: " + data);
        } catch (NullPointerException e) {
            System.out.println("error with msg: " + data);
        } catch (IOException e) {
            System.out.println("error with msg: " + data);
        } catch (ClassCastException e) {
            System.out.println("error with msg: " + data);
        }

    }

    @OnWebSocketConnect
    public void onOpen(Session Session) {
        JsonObject Msg = new JsonObject();
        Msg.addProperty("code", SendCodes.INIT.ordinal());
        Msg.addProperty("mobile", mobilePassword);
        System.out.println(user.getLogin() + " came");
        Msg.add("lobbies", lobbiesList());
        try {
            Session.getRemote().sendString(Msg.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.session = Session;
        GameService.addMenuUser(this);

    }

    public void sendLobbiesList(){
        JsonObject Msg = new JsonObject();
        Msg.addProperty("code", SendCodes.INIT.ordinal());
        Msg.add("lobbies", lobbiesList());
        GameService.sendMenuUsers(Msg);
    }

    public JsonArray lobbiesList()
    {
        JsonArray ListLobbies = new JsonArray();
        for (Map.Entry<String, GameSession> entry : GameService.getOpenLobbies().entrySet()) {
            GameSession Temp = entry.getValue();
            if(!Temp.getStarted())
                ListLobbies.add(Temp.getJson());
        }
        return ListLobbies;
    }

    private JsonObject userCreatingLobby(JsonObject msg) {
        String name = msg.get("name").getAsString();
        int count = msg.get("maxPlayers").getAsInt();
        if (!GameService.lobbyExists(name) && count>0) {
            if (lobby != null)
                lobby.removeUser(this);
            GameSession newLobby = GameService.createLobby(name, this, DB, count);
            this.lobby = newLobby;
            JsonObject lobby = newLobby.getJson();
            lobby.addProperty("code", SendCodes.INFONEWLOBBY.ordinal());
            GameService.sendMenuUsers(lobby);

            lobby.addProperty("code", SendCodes.CREATESUCCESS.ordinal());

            System.out.println(user.getLogin() + " created lobby " + name + " into team " + user.getTeam());

            return lobby;
            // String login = msg.get("login").getAsString();
        } else {
            JsonObject ResponseMsg = new JsonObject();
            ResponseMsg.addProperty("code", SendCodes.CREATEFAILED.ordinal());
            ResponseMsg.addProperty("description", "lobby with this name already created");
            return ResponseMsg;
        }
    }

    private JsonObject userComingLobby(JsonObject msg) {
        String name = msg.get("lobby").getAsString();
        if (!GameService.lobbyExists(name)) {
            JsonObject ResponseMsg = new JsonObject();
            ResponseMsg.addProperty("code", SendCodes.CAMEFAILED.ordinal());
            ResponseMsg.addProperty("description", "lobby with this name already created");
            return ResponseMsg;
            // String login = msg.get("login").getAsString();
        } else {
            GameSession lobby = GameService.getOpenLobbies().get(name);
            this.lobby = lobby;
            if (GameService.userToLobby(this, lobby)) {

                JsonObject msgAnswer = new JsonObject();
                msgAnswer.addProperty("code", SendCodes.NEWUSERLOBBY.ordinal());
                msgAnswer.addProperty("user", user.getLogin());
                msgAnswer.addProperty("team", user.getTeam());

                this.lobby.sendUsers(msgAnswer);
                System.out.println("user " + user.getLogin() + " in lobby " + lobby.getName());
                JsonObject ResponseMsg = new JsonObject();
                ResponseMsg.addProperty("code", SendCodes.CAMESUCCESS.ordinal());
                ResponseMsg.add("users", lobby.usersInLobby());

                return ResponseMsg;
            } else {
                JsonObject ResponseMsg = new JsonObject();
                ResponseMsg.addProperty("code", SendCodes.CAMEFAILED.ordinal());
                ResponseMsg.addProperty("description", "lobby with this name already created");
                return ResponseMsg;
            }
        }
    }

    private void gameStart() {
        if (lobby.getUserCount() == lobby.getMaxCount()) {
            this.lobby.sendStartGame();
            lobby.start();
        } else {
            JsonObject msgAnswer = new JsonObject();
            msgAnswer.addProperty("code", SendCodes.NOTSTARTED.ordinal());
            try {
                session.getRemote().sendString(msgAnswer.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public GamePlayer getPlayer() {
        return user;
    }

    public GameSession getLobby() {
        return lobby;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session Session) {
        this.session = Session;
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        if (lobby == null)
            GameService.removeMenuUser(this);
        else {
            GameService.removeGamer(this);
            lobby.removeUser(this);
            lobby = null;
        }
        auth.deleteProfileSocket(user.getLogin());
        auth.removeMobilePass(mobilePassword);
    }

    public void setMobilePass(String pass)
    {
        mobilePassword = pass;
    }
}