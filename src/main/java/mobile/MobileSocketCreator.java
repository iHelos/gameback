package mobile;

import base.AccountService;
import base.DBService;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.jetbrains.annotations.Nullable;
import websockets.GameSocket;

import java.util.Map;

/**
 * Created by olegermakov on 24.12.15.
 */
public class MobileSocketCreator  implements WebSocketCreator {
    private AccountService authService;

    public MobileSocketCreator(AccountService base) {
        authService = base;
    }

    @Nullable
    @Override
    public Object createWebSocket(ServletUpgradeRequest servletUpgradeRequest, ServletUpgradeResponse servletUpgradeResponse) {
        String password;
        try
        {
            password = servletUpgradeRequest.getParameterMap().get("pass").toString();
            int length = password.length();
            password = password.substring(1,length-1);
        }
        catch (NullPointerException e)
        {
            return null;
        }
        GameSocket game = authService.getMobile(password);
        if(game!=null)
        {
            return new MobileSocket(game);
        }
        return null;
    }
}
