package mobile;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import gameplay.GamePlayer;
import gameplay.GameSession;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import websockets.GameSocket;

/**
 * Created by olegermakov on 24.12.15.
 */
@WebSocket
public class MobileSocket {

    GameSession lobby;
    GamePlayer user;

    GameSocket socket;

    public MobileSocket(GameSocket socket)
    {
        lobby = socket.getLobby();
        user = socket.getPlayer();
        this.socket = socket;
    }

    @SuppressWarnings("OverlyComplexMethod")
    @OnWebSocketMessage
    public void onMessage(String data) {
        try {
            //noinspection TryWithIdenticalCatches
            JsonObject msg = new Gson().fromJson(data, JsonObject.class);
            GameSocket.ReceiveCodes UserCode = getCode(msg.get("code").getAsInt());
            switch (UserCode) {
                case MOVERIGHT:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move right;");
                        user.rightVx();
                    }
                    break;
                case MOVELEFT:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move left;");
                        user.leftVx();
                    }
                    break;
                case MOVEUP:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move up;");
                        user.topVy();
                    }
                    break;
                case MOVEDOWN:
                    if (lobby.getStarted()) {
                        System.out.println(user.getLogin() + " move down;");
                        user.bottomVy();
                    }
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            lobby = socket.getLobby();
            System.out.println("mobile problems");
        }
    }

    private GameSocket.ReceiveCodes getCode(int i) {
        return GameSocket.ReceiveCodes.values()[i];
    }
}
