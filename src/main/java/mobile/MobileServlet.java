package mobile;

import base.AccountService;
import base.DBService;
import database.datasets.UserProfile;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import webanswer.PageGenerator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by olegermakov on 24.12.15.
 */
@WebServlet(name = "MobileServlet", urlPatterns = {"/mobile/"})
public class MobileServlet extends WebSocketServlet {

    private final AccountService authService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> pageVariables = new HashMap<>();

        String password = req.getParameter("pass");

        response.setStatus(HttpServletResponse.SC_OK);
        response.sendRedirect("/mobile.html?pass="+password);
    }

    public MobileServlet(AccountService base) {
        authService = base;
    }
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.setCreator(new MobileSocketCreator(authService));
    }
}
