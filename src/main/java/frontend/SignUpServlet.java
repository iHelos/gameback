package frontend;

import auth.SecurityMD5;
import base.AccountService;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import database.datasets.UserProfile;
import org.jetbrains.annotations.NotNull;
import webanswer.JsonGenerator;
import webanswer.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by olegermakov on 22.09.15.
 */
public class SignUpServlet extends HttpServlet {
    @NotNull
    private AccountService accountService;

    public SignUpServlet(AccountService base) {
        this.accountService = base;
    }

    @Override
    public void doGet(@NotNull HttpServletRequest request,
                      @NotNull HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> pageVariables = new HashMap<>();
        UserProfile profile = accountService.getSessions(request.getSession().getId());
        if (profile == null) {
            response.getWriter().println(PageGenerator.getPage("SignUp.html", pageVariables));
        } else {
            pageVariables.put("status", 1);
            pageVariables.put("description", "already signed up");

            response.setContentType("application/json; charset=utf-8");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
        }
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    public void doPost(@NotNull HttpServletRequest request,
                       @NotNull HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> pageVariables = new HashMap<>();

        JsonObject requestJson;
        try {

            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (IOException | JsonSyntaxException | JsonIOException e) {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 1);
            pageVariables.put("description", "Invalid request type");
            response.addHeader("Content-Type", "application/json");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
            return;
        }

        String name;
        String email;
        String password;

        try {
            name = requestJson.get("name").getAsString().toUpperCase();
            email = requestJson.get("email").getAsString();
            password = requestJson.get("password").getAsString();
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 1);
            pageVariables.put("description", "Bad parameters");
            response.addHeader("Content-Type", "application/json");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
            return;
        }

        //noinspection OverlyComplexBooleanExpression
        if (name == null || email == null || password == null || Objects.equals(name, "") || Objects.equals(password, "")) {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 1);
            pageVariables.put("description", "empty field");
        } else {
            UserProfile newUser = new UserProfile(name, SecurityMD5.getMD5(password), email);

            if (accountService.getUser(name) == null) {
                response.setStatus(HttpServletResponse.SC_OK);
                accountService.addUser(newUser.getLogin(), newUser);
                accountService.addSessions(request.getSession().getId(), newUser);
                pageVariables.put("status", 0);
                pageVariables.put("name", name);
                pageVariables.put("email", email);
            } else {
                response.setStatus(HttpServletResponse.SC_OK);
                pageVariables.put("status", 2);
                pageVariables.put("description", "User with this name already exists!");
            }
        }

        response.addHeader("Content-Type", "application/json");
        response.getWriter().println(JsonGenerator.getJson(pageVariables));
    }
}
