package frontend;

import base.DBService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import database.datasets.UserProfile;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by olegermakov on 23.11.15.
 */
public class StatServlet extends HttpServlet {

    @NotNull
    private DBService Service;

    public StatServlet(DBService base) {
        this.Service = base;
    }

    @Override
    public void doGet(@NotNull HttpServletRequest request,
                      @NotNull HttpServletResponse response) throws ServletException, IOException {

        JsonObject result = new JsonObject();
        ArrayList<UserProfile> list = new ArrayList<>();
        try {
            list = Service.getTen();
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            System.out.println("damn");
            return;
        }


        response.setStatus(HttpServletResponse.SC_OK);
        JsonArray users = new JsonArray();


        for (UserProfile user : list) {
            JsonObject userJSON = new JsonObject();
            userJSON.addProperty("name", user.getLogin());
            // userJSON.addProperty("score", user.getWinCount());
            userJSON.addProperty("score", user.getWinCount());
            userJSON.addProperty("lose", user.getLoseCount());
            userJSON.addProperty("draw", user.getDrawCount());
            users.add(userJSON);
        }
        result.addProperty("status", 0);
        result.add("users", users);
        response.getWriter().println(result);
    }
}
