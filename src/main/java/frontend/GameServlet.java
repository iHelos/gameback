package frontend;

import base.AccountService;
import base.DBService;
import database.datasets.UserProfile;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.jetbrains.annotations.NotNull;
import webanswer.PageGenerator;
import websockets.GameSocketCreator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iHelo on 22.10.2015.
 */

@WebServlet(name = "GameServlet", urlPatterns = {"/game/"})
public class GameServlet extends WebSocketServlet {
    private static final int LOGOUT_TIME = 10 * 60 * 1000;
    private final AccountService authService;
    private DBService DB;

    public GameServlet(AccountService base, DBService DB) {
        authService = base;
        this.DB = DB;
    }

    @Override
    public void doGet(@NotNull HttpServletRequest request,
                      @NotNull HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> pageVariables = new HashMap<>();

        UserProfile profile = authService.getSessions(request.getSession().getId());
        if (profile == null) {
            response.getWriter().println(PageGenerator.getPage("SignIn.html", pageVariables));
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("text/html;charset=utf-8");
            pageVariables.put("name", profile.getLogin());
            pageVariables.put("email", profile.getEmail());
            response.getWriter().println(PageGenerator.getPage("game.html", "templates", pageVariables));
        }
    }

    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.getPolicy().setIdleTimeout(LOGOUT_TIME);
        factory.setCreator(new GameSocketCreator(authService, DB));
    }
}
