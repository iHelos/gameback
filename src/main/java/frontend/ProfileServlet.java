package frontend;

import base.AccountService;
import database.datasets.UserProfile;
import org.jetbrains.annotations.NotNull;
import webanswer.JsonGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by olegermakov on 22.09.15.
 */
public class ProfileServlet extends HttpServlet {
    @NotNull
    private AccountService accountService;

    public ProfileServlet(AccountService base) {
        this.accountService = base;
    }

    @Override
    public void doGet(@NotNull HttpServletRequest request,
                      @NotNull HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> pageVariables = new HashMap<>();

        UserProfile profile = accountService.getSessions(request.getSession().getId());
        if (profile == null) {
            response.setStatus(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
            pageVariables.put("status", 1);
            pageVariables.put("description", "not login");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 0);
            pageVariables.put("name", profile.getLogin());
            pageVariables.put("email", profile.getEmail());
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
        }
    }
}
