package frontend;

import com.google.gson.JsonObject;
import org.jetbrains.annotations.NotNull;
import resourceSystem.ResourceFactory;
import resourceSystem.resourceObjects.LobbyConfig;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by olegermakov on 23.11.15.
 */
public class ConfigServlet extends HttpServlet {

    @Override
    public void doGet(@NotNull HttpServletRequest request,
                      @NotNull HttpServletResponse response) throws ServletException, IOException {

        JsonObject result = new JsonObject();
        LobbyConfig lobbyCfg = (LobbyConfig) ResourceFactory.getInstance().get("data/lobby_config.xml");
        result.addProperty("status", 0);
        result.addProperty("teams", lobbyCfg.getTeams());
        result.addProperty("players", lobbyCfg.getMembers());
        result.addProperty("fieldHeight", lobbyCfg.getFieldHeight());
        result.addProperty("fieldWidth", lobbyCfg.getFieldWidth());
        result.addProperty("gameTime", lobbyCfg.getGameTime());
        result.addProperty("maxSpeed", lobbyCfg.getMaxSpeed());
        result.addProperty("ballRadius", lobbyCfg.getBallRadius());
        result.addProperty("playersRadius", lobbyCfg.getPlayerRadius());
        response.getWriter().println(result);
    }
}
