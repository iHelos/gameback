package frontend;

import base.AccountService;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import database.datasets.UserProfile;
import org.jetbrains.annotations.NotNull;
import webanswer.JsonGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iHelos on 21.09.2015.
 */
public class SignInServlet extends HttpServlet {
    @NotNull
    private AccountService accountService;

    public SignInServlet(@NotNull AccountService base) {
        this.accountService = base;
    }

    @Override
    public void doPost(@NotNull HttpServletRequest request,
                       @NotNull HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> pageVariables = new HashMap<>();

        JsonObject requestJson;
        try {

            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (IOException | JsonSyntaxException | JsonIOException e) {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 1);
            pageVariables.put("description", "Invalid request type");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
            return;
        }

        String name;
        String password;

        try {
            name = requestJson.get("name").getAsString().toUpperCase();
            password = requestJson.get("password").getAsString();
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 1);
            pageVariables.put("description", "Bad parameters");
            response.addHeader("Content-Type", "application/json");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
            return;
        }


        if (name.isEmpty() || password.isEmpty()) {
            pageVariables.put("status", 1);
            pageVariables.put("description", "empty field");
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            UserProfile userInput = accountService.getUser(name);
            if (userInput == null) {
                response.setStatus(HttpServletResponse.SC_OK);
                pageVariables.put("status", 1);
                pageVariables.put("description", "no such user");
            } else if (userInput.comparePasswords(password)) {
                response.setStatus(HttpServletResponse.SC_OK);
                pageVariables.put("status", 0);
                pageVariables.put("name", name);
                pageVariables.put("email", userInput.getEmail());
                accountService.addSessions(request.getSession().getId(), userInput);
            } else {
                response.setStatus(HttpServletResponse.SC_OK);
                pageVariables.put("status", 1);
                pageVariables.put("description", "wrong password");
            }
        }

        response.addHeader("Content-Type", "application/json");
        response.getWriter().println(JsonGenerator.getJson(pageVariables));
    }
}
