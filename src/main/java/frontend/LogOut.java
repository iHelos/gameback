package frontend;

import base.AccountService;
import org.jetbrains.annotations.NotNull;
import webanswer.JsonGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by olegermakov on 22.09.15.
 */
public class LogOut extends HttpServlet {
    @NotNull
    private AccountService accountService;

    public LogOut(@NotNull AccountService base) {
        this.accountService = base;
    }

    @Override
    public void doPost(@NotNull HttpServletRequest request,
                       @NotNull HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> pageVariables = new HashMap<>();

        if (accountService.deleteSessions(request.getSession().getId())) {
            response.setStatus(HttpServletResponse.SC_OK);
            pageVariables.put("status", 0);
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            pageVariables.put("status", 1);
            pageVariables.put("description", "already not logged in");
        }

        response.getWriter().println(JsonGenerator.getJson(pageVariables));
    }
}
