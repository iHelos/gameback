package database.dao;

import database.datasets.DataGame;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by olegermakov on 20.11.15.
 */
public class DataGameDAO {
    private Connection connect;
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String getQuery = "select * from Game where id_game = ?";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String getGameUsers = "select user_name, team, score from UserGame left join UserProfile on id_user_game = user_id where id_game_user = ? order by user_name";

    public DataGameDAO(Connection session) {
        this.connect = session;
    }

    @Nullable
    public DataGame getGame(int id) {
        try (
                PreparedStatement gameState = connect.prepareStatement(getQuery);
        ) {
            gameState.setInt(1, id);
            try (
                    ResultSet gameResult = gameState.executeQuery();
            ) {
                DataGame game = new DataGame();
                if (gameResult.next()) {
                    game.setID(gameResult.getInt(1));
                    game.setTeamWinner(gameResult.getInt(2));
                    try (
                            PreparedStatement state = connect.prepareStatement(getGameUsers)
                    ) {
                        state.setInt(1, id);
                        Map<String, Integer> resultUsersTeam1 = new HashMap<>();
                        Map<String, Integer> resultUsersTeam2 = new HashMap<>();
                        try (
                                ResultSet result = state.executeQuery();
                        ) {
                            while (result.next()) {
                                if (result.getInt("team") == 0) {
                                    resultUsersTeam1.put(result.getString("user_name"), result.getInt("score"));
                                } else {
                                    resultUsersTeam2.put(result.getString("user_name"), result.getInt("score"));
                                }
                            }
                        }
                        game.setTeam1(resultUsersTeam1);
                        game.setTeam2(resultUsersTeam2);
                    } catch (SQLException e) {
                        return null;
                    }
                    return game;
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            return null;
        }
    }
}
