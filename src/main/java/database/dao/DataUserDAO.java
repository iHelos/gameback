package database.dao;

import database.datasets.UserProfile;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Created by olegermakov on 20.11.15.
 */

public class DataUserDAO implements AutoCloseable {

    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String getQuery = "select * from UserProfile where user_name = ?";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String getAllQuery = "select * from UserProfile";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String getTenQuery = "select * from UserProfile order by win_count desc, lose_count asc, draw_count asc limit 10";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String writeUserQuery = "INSERT IGNORE INTO UserProfile(user_name, user_password, user_email, " +
            "win_count, lose_count, draw_count) VALUES (?,?,?,?,?,?)";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String updateWinCount = "update UserProfile set win_count = win_count + 1 where user_name = ?";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String updateLoseCount = "update UserProfile set lose_count = lose_count + 1 where user_name = ?";
    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    String updateDrawCount = "update UserProfile set draw_count = draw_count + 1 where user_name = ?";

    private Connection connect;

    public DataUserDAO(Connection session) {
        this.connect = session;
    }

    @Nullable
    public UserProfile getUser(String name) throws SQLException {
        try (PreparedStatement state = connect.prepareStatement(getQuery)) {
            state.setString(1, name);
            try (ResultSet result = state.executeQuery()) {
                if (result.next()) {
                    return userFromResult(result);
                } else {
                    return null;
                }
            }
        }
    }

    public ArrayList<UserProfile> getAllUsers() throws SQLException {
        return getUsersWithQuery(getAllQuery);
    }

    public ArrayList<UserProfile> getTenUsers() throws SQLException {
        return getUsersWithQuery(getTenQuery);
    }

    public boolean writeUser(UserProfile profile) throws SQLException {
        try (
                PreparedStatement state = connect.prepareStatement(writeUserQuery)
        ) {
            state.setString(1, profile.getLogin());
            state.setString(2, profile.getPasswordMD5());
            state.setString(3, profile.getEmail());
            state.setInt(4, profile.getWinCount());
            state.setInt(5, profile.getLoseCount());
            state.setInt(6, profile.getDrawCount());
            int marker = state.executeUpdate();
            return marker == 1;
        }
    }

    public boolean updateWinCount(UserProfile profile) throws SQLException {
        try (
                PreparedStatement state = connect.prepareStatement(updateWinCount)
        ) {
            state.setString(1, profile.getLogin());
            int marker = state.executeUpdate();
            profile.incrementWinCount();
            return marker == 1;
        }
    }

    public boolean updateLoseCount(UserProfile profile) throws SQLException {
        try (
                PreparedStatement state = connect.prepareStatement(updateLoseCount)
        ) {
            state.setString(1, profile.getLogin());
            int marker = state.executeUpdate();
            profile.incrementLoseCount();
            return marker == 1;
        }
    }

    public boolean updateDrawCount(UserProfile profile) throws SQLException {
        try (PreparedStatement state = connect.prepareStatement(updateDrawCount)) {
            state.setString(1, profile.getLogin());
            int marker = state.executeUpdate();
            profile.incrementDrawfCount();
            return marker == 1;
        }
    }

    private UserProfile userFromResult(ResultSet result) throws SQLException {
        return new UserProfile(
                result.getString("user_name"),
                result.getString("user_password"),
                result.getString("user_email"),
                result.getInt("win_count"),
                result.getInt("lose_count"),
                result.getInt("draw_count")
        );
    }

    private ArrayList<UserProfile> getUsersWithQuery(String query) throws SQLException {
        try (PreparedStatement state = connect.prepareStatement(query)) {
            try (ResultSet result = state.executeQuery()) {
                ArrayList<UserProfile> resultList = new ArrayList<>();
                while (result.next()) {
                    resultList.add(userFromResult(result));
                }
                return resultList;
            }
        }
    }

    @Override
    public void close() throws SQLException
    {
        connect.close();
    }

}
