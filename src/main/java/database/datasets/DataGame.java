package database.datasets;

import java.util.Map;


/**
 * Created by olegermakov on 20.11.15.
 */
public class DataGame {

    private int ID;

    private String Date;

    private Map<String, Integer> team_1;
    private Map<String, Integer> team_2;

    int teamWinner;

    public DataGame() {
    }

    public DataGame(int ID, int teamWinner, Map<String, Integer> team1, Map<String, Integer> team2) {
        this.ID = ID;
        this.teamWinner = teamWinner;
        this.team_1 = team1;
        this.team_2 = team2;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    public void setTeam1(Map<String, Integer> team_1) {
        this.team_1 = team_1;
    }

    public void setTeam2(Map<String, Integer> team_2) {
        this.team_2 = team_2;
    }

    public void setTeamWinner(int teamWinner) {
        this.teamWinner = teamWinner;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getID() {
        return ID;
    }

    public Map<String, Integer> getTeam1() {
        return team_1;
    }

    public Map<String, Integer> getTeam2() {
        return team_2;
    }

    public int getTeamWinner() {
        return teamWinner;
    }

    public String getDate() {
        return Date;
    }
}
