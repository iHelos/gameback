package database.datasets;

import auth.SecurityMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import websockets.GameSocket;

/**
 * Created by iHelos on 20.09.2015.
 */
public class UserProfile {
    @NotNull
    private String login;
    @NotNull
    private String password_md5;
    @NotNull
    private String email;

    @NotNull
    private int ID;

    private GameSocket userSocket = null;

    private int winCount;
    private int loseCount;
    private int drawCount;

    public UserProfile(@NotNull String Login, @NotNull String Password_md5, @NotNull String Email) {
        this.login = Login;
        this.password_md5 = Password_md5;
        this.email = Email;
        winCount = 0;
        loseCount = 0;
        drawCount = 0;
    }

    public UserProfile(@NotNull String Login, @NotNull String Password_md5, @NotNull String Email, int Wins, int Loses, int Draws) {
        this.login = Login;
        this.password_md5 = Password_md5;
        this.email = Email;
        winCount = Wins;
        loseCount = Loses;
        drawCount = Draws;
    }

    protected UserProfile(@NotNull UserProfile someUser) {
        this(someUser.login, someUser.password_md5, someUser.email, someUser.winCount, someUser.loseCount, someUser.drawCount);
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    @NotNull
    public String getPasswordMD5() {
        return password_md5;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public int getGameCount() {
        return winCount + loseCount + drawCount;
    }

    public int incrementWinCount() {
        return ++winCount;
    }

    public int incrementLoseCount() {
        return ++loseCount;
    }

    public int incrementDrawfCount() {
        return ++drawCount;
    }

    public Boolean comparePasswords(String notMD5pass) {
        return SecurityMD5.getMD5(notMD5pass).equals(password_md5);
    }

    public void setUserSocket(@Nullable GameSocket socket) {
        this.userSocket = socket;
    }

    public GameSocket getUserSocket() {
        return userSocket;
    }

}
