package database;

import base.DBService;
import database.dao.DataUserDAO;
import database.datasets.UserProfile;
import org.apache.commons.dbcp.BasicDataSource;
import resourceSystem.resourceObjects.ServerConfig;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by olegermakov on 20.11.15.
 */
public class DBServiceImpl implements DBService {
    //private static DBServiceImpl     datasource;
    private BasicDataSource ds;
    private String DBName;

    public DBServiceImpl() throws IOException, SQLException, PropertyVetoException {
        ServerConfig serverConfig = ServerConfig.getInstance();
        DBName = serverConfig.getDB_name();

        ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername(serverConfig.getDB_user());
        ds.setPassword(serverConfig.getDB_pass());
        ds.setUrl("jdbc:mysql://" + serverConfig.getDB_ip() + ':' + serverConfig.getDB_port() + '/' + "?autoreconnect=true&useUnicode=yes&characterEncoding=UTF-8");
        ds.setTestOnBorrow(true);
        ds.setValidationQuery("Select 1");
        ds.setTestOnReturn(true);
        configDatabase();
        configTables();

    }

    public Connection getConnection() throws SQLException {
        Connection con = this.ds.getConnection();
        con.setCatalog(DBName);
        return con;
    }

    private void autoDBCreation(Connection connect, Statement statement) throws SQLException, IOException {
        statement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + DBName);
    }

    private void userTableCreation(Statement statement) throws SQLException, IOException {
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS " +
                DBName +
                ".UserProfile (" +
                "        user_id int(11) NOT NULL AUTO_INCREMENT," +
                "        user_name varchar(100) NOT NULL," +
                "        user_password varchar(100) NOT NULL," +
                "        user_email varchar(255) NOT NULL," +
                "        win_count int(11) NOT NULL DEFAULT '0'," +
                "        lose_count int(11) NOT NULL DEFAULT '0'," +
                "        draw_count int(11) NOT NULL DEFAULT '0'," +
                "            PRIMARY KEY (user_id)," +
                "            UNIQUE KEY user_name_UNIQUE (user_name)," +
                "            KEY user_email (user_email)" +
                "        ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;");
    }


    private void configDatabase() throws IOException, SQLException {
        try (
                Connection con = ds.getConnection();
                Statement statement = con.createStatement();
        ) {
            autoDBCreation(con, statement);
        }

    }

    private void configTables() throws IOException, SQLException {
        try (
                Connection con = ds.getConnection();
                Statement statement = con.createStatement()
        ) {
            userTableCreation(statement);
        }
    }

    @Override
    public ArrayList<UserProfile> getUsers() throws SQLException {
        try (DataUserDAO userDAO = new DataUserDAO(getConnection())) {
            return userDAO.getAllUsers();
        }
    }

    @Override
    public ArrayList<UserProfile> getTen() throws SQLException {
        try (DataUserDAO userDAO = new DataUserDAO(getConnection())) {
            return userDAO.getTenUsers();
        }
    }

    @Override
    public boolean addUser(UserProfile user) throws SQLException {
        try (DataUserDAO userDAO = new DataUserDAO(getConnection())) {
            return userDAO.writeUser(user);
        }
    }

    @Override
    public void shutdown() throws SQLException {
        ds.close();
    }

    @Override
    public void setWin(UserProfile user) throws SQLException {
        try (DataUserDAO userDAO = new DataUserDAO(getConnection())) {
            userDAO.updateWinCount(user);
        }
    }

    @Override
    public void setLose(UserProfile user) throws SQLException {
        try (DataUserDAO userDAO = new DataUserDAO(getConnection())) {
            userDAO.updateLoseCount(user);
        }
    }

    @Override
    public void setDraw(UserProfile user) throws SQLException {
        try (DataUserDAO userDAO = new DataUserDAO(getConnection())) {
            userDAO.updateDrawCount(user);
        }
    }

}
