package parse;

import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by iHelo on 22.10.2015.
 */
public class PropertiesParser {

    @SuppressWarnings("OverlyBroadThrowsClause")
    @NotNull
    public static Properties getProperties(@NotNull String path) throws IOException {
        Properties properties = new Properties();
        final FileInputStream fis = new FileInputStream(path);
        properties.load(fis);
        return properties;
    }
}
