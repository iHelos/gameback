package admin;

import base.AccountService;
import base.GameCycle;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.jetbrains.annotations.NotNull;
import webanswer.JsonGenerator;
import webanswer.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by olegermakov on 22.09.15.
 */
public class AdminServlet extends HttpServlet {
    @NotNull
    private final AccountService accountService;

    @NotNull
    private final GameCycle gameCycle;

    public AdminServlet(AccountService base, GameCycle game) {
        this.accountService = base;
        this.gameCycle = game;
    }

    @Override
    public void doGet(@NotNull HttpServletRequest request,
                      @NotNull HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> pageVariables = new HashMap<>();


        if (accountService.getSessions(request.getSession().getId()) != null && Objects.equals(accountService.getSessions(request.getSession().getId()).getLogin(), "admin")) {

            response.setContentType("text/html;charset=utf-8");

            pageVariables.put("RegCount", accountService.getRegisteredCount());
            pageVariables.put("LogCount", accountService.getLoggedCount());
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(PageGenerator.getPage("Admin.html", pageVariables));
        } else {
            pageVariables.put("status", 1);
            pageVariables.put("description", "no permission");

            response.getWriter().println(JsonGenerator.getJson(pageVariables));
        }
    }

    @Override
    public void doPost(@NotNull HttpServletRequest request,
                       @NotNull HttpServletResponse response) throws ServletException, IOException, NullPointerException {

        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        Map<String, Object> pageVariables = new HashMap<>();


        if (accountService.getSessions(request.getSession().getId()) != null && Objects.equals(accountService.getSessions(request.getSession().getId()).getLogin(), "admin")) {

            JsonObject requestJson;
            try {

                requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
            } catch (IOException | JsonSyntaxException | JsonIOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                pageVariables.put("status", 1);
                pageVariables.put("description", "Invalid request type");
                response.getWriter().println(JsonGenerator.getJson(pageVariables));
                return;
            }

            int time;

            try {
                time = requestJson.get("stop").getAsInt();
            } catch (RuntimeException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                pageVariables.put("status", 1);
                pageVariables.put("description", "Bad parameters");
                response.getWriter().println(JsonGenerator.getJson(pageVariables));
                return;
            }

            System.out.print("Server will be down after: " + time + " ms");
            SleepThread.sleep(time);
            System.out.print("\nShutdown");
            gameCycle.stop();
            System.exit(0);
        } else {
            pageVariables.put("status", 1);
            pageVariables.put("description", "no permission");
            response.getWriter().println(JsonGenerator.getJson(pageVariables));
        }
    }
}