package webanswer;
/**
 * Created by olegermakov on 14.09.15.
 */

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class JsonGenerator {
    /* TODO */

    @NotNull
    public static JsonObject getJson(@NotNull Map<String, Object> data) {
        Gson gson = new Gson();
        JsonObject result = new JsonObject();
        result.addProperty("code", (int) data.get("status"));
        data.remove("status");

        JsonElement jsonElement = gson.toJsonTree(data);
        result.add("response", jsonElement);

        /*
        for (Map.Entry<String, Object> entry : data.entrySet()) {

            body.addProperty(entry.getKey(), entry.getValue());
        }
        */
        return result;
    }
}