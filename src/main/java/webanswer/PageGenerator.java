package webanswer;
/**
 * Created by olegermakov on 15.09.15.
 */

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

public class PageGenerator {

    @NotNull
    private static final String HTMLDIT = "templates";
    @NotNull
    private static Configuration scfg = new Configuration();

    @NotNull
    public static Object getPage(@NotNull String filename, Map<String, Object> data) {
        Writer stream = new StringWriter();
        try {
            Template template = scfg.getTemplate(HTMLDIT + File.separator + filename);
            template.process(data, stream);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
        return stream;
    }

    @NotNull
    public static Object getPage(@NotNull String filename, @NotNull String directory, Map<String, Object> data) {
        Writer stream = new StringWriter();
        try {
            Template template = scfg.getTemplate(directory + File.separator + filename);
            template.process(data, stream);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
        return stream;
    }
}
