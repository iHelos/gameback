package gameplay;

import admin.SleepThread;
import base.GameCycle;
import resourceSystem.resourceObjects.LobbyConfig;
import resourceSystem.ResourceFactory;
import websockets.WebSocketServiceImpl;

import java.util.Map;

/**
 * Created by olegermakov on 01.11.15.
 */
public class GameCycleImpl implements GameCycle {
    private static final int STEP_TIME = 1000/60;
    private static Boolean running = true;
    @Override
    public void run() {
        LobbyConfig lobbyCfg = (LobbyConfig) ResourceFactory.getInstance().get("data/lobby_config.xml");
        GameSession.setSessionProperties(lobbyCfg.getFieldWidth(), lobbyCfg.getFieldHeight(), lobbyCfg.getMaxSpeed(), lobbyCfg.getBallRadius(), lobbyCfg.getGameTime());
        WebSocketServiceImpl.setProperties(lobbyCfg.getTeams(), lobbyCfg.getMembers());
        GamePlayer.setRadius(lobbyCfg.getPlayerRadius());

        while(true) {
            sessionStep();
            SleepThread.sleep(STEP_TIME);
        }
    }

    @Override
    public void sessionStep() {
        Map <String, GameSession> lobbies = WebSocketServiceImpl.getInstance().getOpenLobbies();
        for (Map.Entry<String, GameSession> entry : lobbies.entrySet()) {
            GameSession lobby = entry.getValue();
            if(lobby.getStarted()) {
                lobby.makeStep();
                lobby.worldUpdate();
            }
        }
    }

    @Override
    public void stop()
    {
        running = false;
    }

}
