package gameplay;

import base.DBService;
import com.google.gson.JsonArray;
import org.jetbrains.annotations.NotNull;
import resourceSystem.ResourceFactory;
import resourceSystem.resourceObjects.LobbyConfig;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by iHelo on 22.10.2015.
 */
public class GameTeam {

    DBService DB;

    public GameTeam(@NotNull String Name, int ID, DBService DB) {
        this.DB = DB;
        TeamName = Name;
        id = ID;
        teamMembers = new ArrayList<>();
    }

    @NotNull
    private ArrayList<GamePlayer> teamMembers;
    @NotNull
    private String TeamName;
    private int id;

    public void addMember(@NotNull GamePlayer player) {
        teamMembers.add(player);
        player.setTeam(id);
    }

    public void removeMember(@NotNull GamePlayer player) {
        teamMembers.remove(player);
    }

    public int getTeamScore() {
        int teamScore = 0;
        for (GamePlayer user : teamMembers) {
            teamScore += user.getUserScore();
        }
        return teamScore;
    }

    public int getCount() {
        return teamMembers.size();
    }

    public String getName() {
        return TeamName;
    }

    public int getID() {
        return id;
    }

    public JsonArray getTeamMembers() {
        JsonArray listMembers = new JsonArray();
        for (GamePlayer user : teamMembers) {
            listMembers.add(user.toJson());
        }
        return listMembers;
    }

    public JsonArray getTeamMembersLogins() {
        JsonArray listMembers = new JsonArray();
        for (GamePlayer user : teamMembers) {
            listMembers.add(user.getLogin());
        }
        return listMembers;
    }

    public int newUserX() {
        return coordinateByNumber(teamMembers.size());
    }

    public int coordinateByNumber(int ID)
    {
        LobbyConfig lobbyCfg = (LobbyConfig) ResourceFactory.getInstance().get("data/lobby_config.xml");
        int radius = lobbyCfg.getPlayerRadius();
        return ((ID + 1) * radius * 3) * (((id * 2) - 1) * (-1));
    }

    public void setStartCoordinates()
    {
        LobbyConfig lobbyCfg = (LobbyConfig) ResourceFactory.getInstance().get("data/lobby_config.xml");
        int height = lobbyCfg.getFieldHeight();
        int width = lobbyCfg.getFieldWidth();
        int i = 0;
        for(GamePlayer teamplayer : teamMembers)
        {
            teamplayer.setY(height / 2);
            teamplayer.setX(width/2 + coordinateByNumber(i++));
        }
    }

    public void clear() {
        teamMembers.clear();
    }

    public void setWin(){
        try {
            for (GamePlayer user : teamMembers) {
                DB.setWin(user);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

        }

    }

    public void setLose(){
        try {
            for (GamePlayer user : teamMembers) {
                DB.setLose(user);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void setDraw(){
        try {
            for (GamePlayer user : teamMembers) {
                DB.setDraw(user);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
