package gameplay;

import base.DBService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.eclipse.jetty.websocket.api.WebSocketException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import websockets.GameSocket;
import websockets.WebSocketServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by iHelo on 22.10.2015.
 */

public class GameSession {
    private static int maxSpeed;

    private static int width;
    private static int height;

    private static int gameTime;

    @NotNull
    private Boolean isStarted;
    private long startTime;

    private Ball ball;

    @NotNull
    private String Name;
    @NotNull
    private List<GameSocket> users;
    @NotNull
    private List<GameSocket> gamers;
//    @NotNull
//    private ArrayList<GamePlayer> usersBalls;

    private int maxCount;

    @NotNull
    private List<GameTeam> teams;

    public static void setSessionProperties(int width, int height, int speed, int ballRadius, int GameTime) {
        GameSession.width = width;
        GameSession.height = height;
        GameSession.maxSpeed = speed;
        GameSession.gameTime = GameTime;
        Ball.setRadius(ballRadius);
    }

    public static int getHeight() {
        return height;
    }

    public static int getWidth() {
        return width;
    }

    public static int getMaxSpeed() {
        return maxSpeed;
    }

    public GameSession(@NotNull String LobbyName, int countTeams, int MaxCount, DBService DB) {
        users = new CopyOnWriteArrayList<>();
        gamers = new CopyOnWriteArrayList<>();
       // usersBalls = new ArrayList<>();
        teams = new CopyOnWriteArrayList<>();
        ball = new Ball();
        for (int i = 0; i < countTeams; i++) {
            teams.add(i, new GameTeam("team" + i, i, DB));
            System.out.println("team" + i);
        }
        Name = LobbyName;
        isStarted = false;
        maxCount = MaxCount;
    }

    public void changeTeam(@NotNull GamePlayer user, int newID) {
        if (!isStarted) {
            GameTeam OldTeam = teams.get(user.getTeam());
            GameTeam NewTeam = teams.get(newID);
            OldTeam.removeMember(user);
            NewTeam.addMember(user);
        }
    }

    public int addUser(@NotNull GameSocket userPr) {
        if (getUserCount() < maxCount && !isStarted) {
            GameTeam Team = getMinCountTeam();
            GamePlayer gp = userPr.getPlayer();
//            gp.setX(width / 2 + Team.newUserX());
//            gp.setY(height / 2);

            Team.addMember(gp);
            users.add(userPr);
            gamers.add(userPr);

            int rotation = Team.getID()*2 - 1;
            gp.setVx(0 * rotation);
            gp.setVy(0 * rotation);
          //  usersBalls.add(gp);
            return Team.getID();
        } else {
            //TODO либо режим спектатора, либо просто октаз на вход
            try {
                userPr.getSession().getRemote().sendString(usersList().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int getUserCount() {
        return users.size();
    }

    @NotNull
    private GameTeam getMinCountTeam() {
        GameTeam minCountTeam = teams.get(0);
        int minCount = minCountTeam.getCount();
        for (GameTeam team : teams) {

            int newsize = team.getCount();
            if (newsize < minCount) {
                minCount = newsize;
                minCountTeam = team;
            }
        }
        return minCountTeam;
    }

    @NotNull
    private GameTeam getMaxScoreTeam() {
        GameTeam maxTeam = teams.get(1);
        int maxScore = 0;
        for (GameTeam team : teams) {
            int newscore = team.getTeamScore();
            if (newscore > maxScore) {
                maxScore = newscore;
                maxTeam = team;
            }
        }
        return maxTeam;
    }

    @Nullable
    private int getMaxScoreTeamFrom2() {
        GameTeam team1 = teams.get(0);
        GameTeam team2 = teams.get(1);
        int score1 = team1.getTeamScore();
        int score2 = team2.getTeamScore();
        if (score1 > score2) {
            team1.setWin();
            team2.setLose();
            return team1.getID();
        }
        else if (score2 > score1) {
            team1.setLose();
            team2.setWin();
            return team2.getID();
        }
        else {
            team1.setDraw();
            team2.setDraw();
            return 3;
        }
    }

    public long getSessionTime() {
        return new Date().getTime() - startTime;
    }

    public void start() {
        isStarted = true;
        startTime = new Date().getTime();
    }

    public int getWinner() {
        //return getMaxScoreTeam().getID();
        return getMaxScoreTeamFrom2();
    }

    public int getMaxCount() {
        return maxCount;
    }

    public JsonObject getJson() {
        JsonObject lobby = new JsonObject();
        lobby.addProperty("name", Name);
        lobby.addProperty("isStarted", isStarted);
        lobby.addProperty("count", this.getUserCount());
        lobby.addProperty("maxCount", maxCount);
        return lobby;
    }

    @NotNull
    public String getName() {
        return Name;
    }

    @NotNull
    public Boolean getStarted() {
        return isStarted;
    }

    public JsonObject usersList() {
        JsonObject teamMembers = new JsonObject();
        for (int i = 0; i < teams.size(); i++) {
            teamMembers.add(String.valueOf(i), teams.get(i).getTeamMembers());
        }

        return teamMembers;
    }

    public JsonObject usersInLobby() {
        JsonObject teamMembers = new JsonObject();
        for (int i = 0; i < teams.size(); i++) {
            teamMembers.add(String.valueOf(i), teams.get(i).getTeamMembersLogins());
        }

        return teamMembers;
    }

    public void sendUsers(JsonObject data) {
        for (GameSocket user : users) {
            try {
                user.getSession().getRemote().sendString(data.toString());
            } catch (IOException | WebSocketException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendStartGame() {
        for(GameTeam team : teams)
            team.setStartCoordinates();
        for (GameSocket user : users) {
            try {
                JsonObject result = new JsonObject();
                result.addProperty("code", GameSocket.SendCodes.STARTINFO.ordinal());
                JsonArray balls = new JsonArray();
                balls.add(ball.toJson());
                for (GameSocket jsonUser : users) {
                    JsonObject tempUser = jsonUser.getPlayer().toJson();
                    if (jsonUser == user)
                        tempUser.addProperty("self", true);
                    else
                        tempUser.addProperty("self", false);
                    tempUser.addProperty("name", jsonUser.getPlayer().getLogin());
                    tempUser.addProperty("team", jsonUser.getPlayer().getTeam());
                    balls.add(tempUser);
                }
                result.add("balls", balls);
                user.getSession().getRemote().sendString(result.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public JsonObject worldInfo() {
        JsonObject result = new JsonObject();
        result.addProperty("code", GameSocket.SendCodes.WORLDINFO.ordinal());
        JsonArray balls = new JsonArray();
        balls.add(ball.toJson());
        for (GameSocket user : gamers) {
            balls.add(user.getPlayer().toJson());
        }
        result.add("balls", balls);
        return result;
    }

    public void worldUpdate() {
        sendUsers(worldInfo());
    }

    public void makeStep() {
        if (getSessionTime() <= 1000 * gameTime) {
            int index = 0;
            for (GameSocket user : gamers) {
                user.getPlayer().updateCoordinates();
                collisionBall(index++);
            }
            ball.updateCoordinates(this);
            for (int j = 1; j < gamers.size(); ++j) {
                for (int i = j - 1; i >= 0; i--) {
                    collisionPlayers(i, j);
                }
            }
        } else {
            JsonObject result = new JsonObject();
            result.addProperty("code", GameSocket.SendCodes.GAMEOVER.ordinal());
            result.addProperty("team", getWinner());
            sendUsers(result);
            toDefault();
        }
    }

    private void collisionPlayers(int first, int second) {
        GamePlayer firstPlayer = gamers.get(first).getPlayer();
        GamePlayer secondPlayer = gamers.get(second).getPlayer();

        int deltaX = firstPlayer.getX() - secondPlayer.getX();
        int deltaY = firstPlayer.getY() - secondPlayer.getY();

        double distanceSqr = deltaX * deltaX + deltaY * deltaY;
        double minDistance = 2 * GamePlayer.getRadius();

        if (distanceSqr < minDistance * minDistance) {
            GamePlayer.changeSpeed(firstPlayer, secondPlayer);
        }
    }

    public void collisionBall(int index) {
        GamePlayer firstPlayer = gamers.get(index).getPlayer();
        int deltaX = firstPlayer.getX() - ball.getX();
        int deltaY = firstPlayer.getY() - ball.getY();
        double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        double minDistance = GamePlayer.getRadius() + Ball.getRadius();

        if (distance < minDistance) {
            ball.setLastHitted(firstPlayer);
            int playerSpeedX = firstPlayer.getVx();
            int playerSpeedY = firstPlayer.getVy();
            int ballSpeedX = ball.getVx();
            int ballSpeedY = ball.getVy();

            int deltaVX = playerSpeedX - ballSpeedX;
            int deltaVY = playerSpeedY - ballSpeedY;

            int newBallSpeedX = ballSpeedX + 2 * deltaVX;
            int newBallSpeedY = ballSpeedY + 2 * deltaVY;

            ball.setVx(newBallSpeedX);
            ball.setVy(newBallSpeedY);
            ball.updateCoordinates(this);

            if (newBallSpeedX != 0)
                firstPlayer.setVx(-newBallSpeedX / Math.abs(newBallSpeedX));
            if (newBallSpeedY != 0)
                firstPlayer.setVy(-newBallSpeedY / Math.abs(newBallSpeedY));

            firstPlayer.updateCoordinates();
        }
    }

    public void updateScore() {
        JsonObject result = new JsonObject();
        result.addProperty("code", GameSocket.SendCodes.GOAL.ordinal());
        for (GameTeam team : teams) {
            result.addProperty(team.getName(), team.getTeamScore());
        }
        sendUsers(result);
    }

    public void removeUser(GameSocket player) {

        GamePlayer gamePlayer = player.getPlayer();
        gamePlayer.setGamescore(0);
        users.remove(player);
        JsonObject response = new JsonObject();
        response.addProperty("code", GameSocket.SendCodes.USERLEFTLOBBY.ordinal());
        response.addProperty("name", player.getPlayer().getLogin());
        this.sendUsers(response);
        if (!isStarted) {
            gamers.remove(player);
            teams.get(gamePlayer.getTeam()).removeMember(gamePlayer);
            System.out.println(player.getPlayer().getLogin() + " exited lobby " + Name);
        } else {
            gamePlayer.setX(-width);
            gamePlayer.setY(-height);
            gamePlayer.setVx(0);
            gamePlayer.setVy(0);
            System.out.println(player.getPlayer().getLogin() + " abondoned started game " + Name);
        }
        if (this.getUserCount() == 0)
            WebSocketServiceImpl.getInstance().removeLobby(this);
    }

    public void toDefault() {
        isStarted = false;
        ball = new Ball();
        users.clear();
        for (GameSocket gamer : gamers) {
            gamer.getPlayer().setGamescore(0);
        }
        gamers.clear();
       // usersBalls.clear();
        teams.forEach(GameTeam::clear);
    }

    public void autoGameStart() {
        this.sendStartGame();
        this.start();
    }
}