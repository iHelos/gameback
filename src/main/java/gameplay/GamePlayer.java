package gameplay;

import com.google.gson.JsonObject;
import database.datasets.UserProfile;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by iHelo on 22.10.2015.
 */
public class GamePlayer extends UserProfile {

    private static int radius = 35;

    public GamePlayer(@NotNull UserProfile SuperUser) {
        super(SuperUser);
        statistics = SuperUser.getWinCount();
        gamescore = 0;

        x = new AtomicInteger(0);
        y = new AtomicInteger(0);
        vx = new AtomicInteger(0);
        vy = new AtomicInteger(0);
    }

    private AtomicInteger x;
    private AtomicInteger y;
    private AtomicInteger vx;
    private AtomicInteger vy;

    private int statistics;
    private int gamescore;
    private int team;

    public int getUserScore() {
        return gamescore;
    }

    public int getStatistics() {
        return statistics;
    }

    public void setTeam(int Team) {
        team = Team;
    }

    public int getTeam() {
        return team;
    }

    public void rightVx() {
        if (vx.get() < GameSession.getMaxSpeed()) {
            if (vx.incrementAndGet() == 0)
                vx.incrementAndGet();
        }
    }

    public void leftVx() {
        if (vx.get() > -GameSession.getMaxSpeed()) {
            if (vx.decrementAndGet() == 0)
                vx.decrementAndGet();
        }
    }

    public void topVy() {
        if (vy.get() < GameSession.getMaxSpeed()) {
            if (vy.incrementAndGet() == 0)
                vy.incrementAndGet();
        }
    }

    public void bottomVy() {
        if (vy.get() > -GameSession.getMaxSpeed()) {
            if (vy.decrementAndGet() == 0)
                vy.decrementAndGet();
        }
    }


    public int incrementUserScore() {
        return ++gamescore;
    }

    public int decrementUserScore() {
        return --gamescore;
    }

    public void updateCoordinates() {
        x.addAndGet(vx.get());
        y.addAndGet(vy.get());
        if (getX() >= GameSession.getWidth() - radius || getX() <= radius) {
            setVx(-getVx());
            x.addAndGet(vx.get());
        }
        if (getY() >= GameSession.getHeight() - radius || getY() <= radius) {
            setVy(-getVy());
            y.addAndGet(vy.get());
        }
    }

    public JsonObject toJson() {
        JsonObject result = new JsonObject();
        result.addProperty("x", x);
        result.addProperty("y", y);
        result.addProperty("vx", vx);
        result.addProperty("vy", vy);
        return result;
    }

    public static void changeSpeed(GamePlayer first, GamePlayer second) {
        int speedFirstVX = first.vx.getAndSet(second.vx.intValue());
        int speedFirstVY = first.vy.getAndSet(second.vy.intValue());
        second.vx.set(speedFirstVX);
        second.vy.set(speedFirstVY);

        first.updateCoordinates();
        second.updateCoordinates();
    }

    public int getX() {
        return x.intValue();
    }

    public int getY() {
        return y.intValue();
    }

    public void setX(int x) {
        this.x.set(x);
    }

    public void setY(int y) {
        this.y.set(y);
    }

    public void setVx(int vx) {
        this.vx.set(vx);
    }

    public void setVy(int vy) {
        this.vy.set(vy);
    }

    public int getVx() {
        return vx.get();
    }

    public int getVy() {
        return vy.get();
    }

    public static void setRadius(int radius) {
        GamePlayer.radius = radius;
    }

    public static int getRadius() {
        return radius;
    }

    public void setGamescore(int gamescore) {
        this.gamescore = gamescore;
    }
}
