package gameplay;

import com.google.gson.JsonObject;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by iHelo on 15.11.2015.
 */
public class Ball {
    private static int ballRadius;

    private AtomicInteger x;
    private AtomicInteger y;
    private AtomicInteger vx;
    private AtomicInteger vy;

    private GamePlayer lastEnemyHitted;
    private GamePlayer lastHitted;

    Ball() {
        x = new AtomicInteger(GameSession.getWidth() / 2);
        y = new AtomicInteger(GameSession.getHeight() / 2);
        vx = new AtomicInteger(0);
        vy = new AtomicInteger(0);
        lastEnemyHitted = null;
        lastHitted = null;
    }

    public JsonObject toJson() {
        JsonObject result = new JsonObject();
        result.addProperty("x", x);
        result.addProperty("y", y);
        result.addProperty("vx", vx);
        result.addProperty("vy", vy);
        return result;
    }

    public int getX() {
        return x.intValue();
    }

    public int getY() {
        return y.intValue();
    }

    public void setVx(int vx) {
        if (vx > GameSession.getMaxSpeed())
            this.vx.set(GameSession.getMaxSpeed());
        else if (vx < -GameSession.getMaxSpeed())
            this.vx.set(-GameSession.getMaxSpeed());
        else
            this.vx.set(vx);
    }

    public void setVy(int vy) {
        if (vy > GameSession.getMaxSpeed())
            this.vy.set(GameSession.getMaxSpeed());
        else if (vy < -GameSession.getMaxSpeed())
            this.vy.set(-GameSession.getMaxSpeed());
        else
            this.vy.set(vy);
    }

    public int getVx() {
        return vx.get();
    }

    public int getVy() {
        return vy.get();
    }

    public void setLastHitted(GamePlayer player) {
        lastHitted = player;
    }

    public GamePlayer getLastHitted() {
        return lastHitted;
    }

    public void collisionGoal(GameSession session) {
        GamePlayer player = lastHitted;
        int playerTeam = player.getTeam();

        int heightGame = GameSession.getHeight();
        int widthGame = GameSession.getWidth();

        int thisX = this.getX();
        int thisY = this.getY();

        if (thisY < (heightGame * 4 / 5) && thisY > (heightGame / 5)) {
            if ((thisX < widthGame / 2 && playerTeam == 0) || (thisX > widthGame / 2 && playerTeam == 1)) {
                lastHitted.incrementUserScore();
            }
            if ((thisX > widthGame / 2 && playerTeam == 0) || (thisX < widthGame / 2 && playerTeam == 1)) {
                lastHitted.decrementUserScore();
            }
            session.updateScore();
        }
    }

    public void updateCoordinates(GameSession gs) {
        x.addAndGet(vx.get());
        y.addAndGet(vy.get());
        if (this.getX() >= GameSession.getWidth() - ballRadius || this.getX() <= ballRadius) {
            collisionGoal(gs);
            this.setVx(-this.getVx());
            x.addAndGet(3 * vx.get());
        }
        if (this.getY() >= GameSession.getHeight() - ballRadius || this.getY() <= ballRadius) {
            this.setVy(-this.getVy());
            y.addAndGet(3 * vy.get());
        }

    }

    public static void setRadius(int radius) {
        ballRadius = radius;
    }

    public static int getRadius() {
        return ballRadius;
    }

}