package resourceSystem;

import base.Resource;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ResourceFactory {

    private static ResourceFactory instance;
    private Map<String, Resource> resourceMap = new HashMap<>();

    private ResourceFactory() {
    }

    public static ResourceFactory getInstance() {
        if (instance == null) {
            instance = new ResourceFactory();
        }
        return instance;
    }

    public Resource get(String path) {
        Resource resource = resourceMap.get(path);
        if (resource == null) {
            System.out.println("No resource on path given");
        }
        return resource;
    }

    public void init(String directory) {
        Iterator<String> iterator = VFSImpl.getInstance().getIterator(directory);
        while (iterator.hasNext()) {
            String path = iterator.next();
            resourceMap.put(path, readXML(path));
        }
    }

    private Resource readXML(String xmlFile) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            SaxHandler handler = new SaxHandler();
            saxParser.parse(xmlFile, handler);

            return (Resource) handler.getObject();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}

