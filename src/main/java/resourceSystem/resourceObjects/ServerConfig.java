package resourceSystem.resourceObjects;

import base.Resource;
import parse.PropertiesParser;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by olegermakov on 23.11.15.
 */
public class ServerConfig implements Resource {
    private int port;
    private String DB_user;
    private String DB_pass;
    private String DB_ip;
    private String DB_port;
    private String DB_name;

    private static volatile ServerConfig instance;

    private ServerConfig() throws IOException, NumberFormatException {
        Properties serverStart = PropertiesParser.getProperties("cfg/server.properties");
        port = Integer.parseInt(serverStart.getProperty("port", "8080"));
        DB_user = serverStart.getProperty("DB_user", "root");
        DB_pass = serverStart.getProperty("DB_pass", "12345");
        DB_ip = serverStart.getProperty("DB_ip", "127.0.0.1");
        DB_port = serverStart.getProperty("DB_port", "3306");
        DB_name = serverStart.getProperty("DB_name", "Football");
    }

    public static ServerConfig getInstance() throws IOException, NumberFormatException {
        ServerConfig localInstance = instance;
        if (localInstance == null) {
            synchronized (ServerConfig.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ServerConfig();
                }
            }
        }
        return localInstance;
    }

    public int getPort() {
        return port;
    }

    public String getDB_ip() {
        return DB_ip;
    }

    public String getDB_pass() {
        return DB_pass;
    }

    public String getDB_port() {
        return DB_port;
    }

    public String getDB_user() {
        return DB_user;
    }

    public String getDB_name() {
        return DB_name;
    }
}
