package resourceSystem.resourceObjects;

import base.Resource;

public class LobbyConfig implements Resource {

    private int teams;
    private int members;
    private int maxSpeed;
    private int fieldWidth;
    private int fieldHeight;
    private int ballRadius;
    private int playerRadius;
    private int gameTime;

    public LobbyConfig() {

    }

    public int getTeams() {
        return teams;
    }

    public int getMembers() {
        return members;
    }

    public int getFieldHeight() {
        return fieldHeight;
    }

    public int getFieldWidth() {
        return fieldWidth;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getBallRadius() {
        return ballRadius;
    }

    public int getPlayerRadius() {
        return playerRadius;
    }

    public int getGameTime() {
        return gameTime;
    }
}
