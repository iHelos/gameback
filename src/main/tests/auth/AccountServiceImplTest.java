package auth;

import base.AccountService;
import base.DBService;
import database.DBServiceImpl;
import database.datasets.UserProfile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.PreparedStatement;

import static junit.framework.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by olegermakov on 30.10.15.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    private DBService DB;
    private AccountService accountService;

    @Mock
    private UserProfile user;

    @Before
    public void setUp() throws Exception {
        DB = new DBServiceImpl();
        when(user.getLogin()).thenReturn("UserTest");
        when(user.getEmail()).thenReturn("tom@mail.com");
        when(user.getPasswordMD5()).thenReturn("12345");
       accountService = new AccountServiceImpl(DB);
    }


    @Test
    public void testAddUser() throws Exception {
        assertTrue(accountService.addUser(user.getLogin(), this.user));
        assertFalse(accountService.addUser(user.getLogin(), this.user));
    }



    @Test
    public void testAddSessions() throws Exception {
        accountService.addSessions("TestSession", user);
        assertNull(accountService.getSessions("ThereIsNoSuchSession"));
        assertNotNull(accountService.getSessions("TestSession"));
    }

    @Test
    public void testDeleteSessions() throws Exception {
        assertFalse(accountService.deleteSessions("12345"));
        accountService.addSessions("TestSession", user);
        assertTrue(accountService.deleteSessions("TestSession"));
    }

    @Test
    public void testGetUser() throws Exception {
        accountService.addUser(user.getLogin(), this.user);
        assertNotNull(accountService.getUser(user.getLogin()));
    }

    @Test
    public void testGetSessions() throws Exception {
        accountService.addSessions("TestSession", user);
        assertEquals(accountService.getSessions("TestSession"), user);
    }

    @Test
    public void testGetRegisteredCount() throws Exception {
        int firstCount =  accountService.getRegisteredCount();
        accountService.addUser(user.getLogin(), this.user);
        assertEquals(firstCount + 1, accountService.getRegisteredCount());
    }

    @Test
    public void testGetLoggedCount() throws Exception {
        int firstCount = accountService.getLoggedCount();
        accountService.addSessions("TestSession", this.user);
        assertEquals(firstCount + 1, accountService.getLoggedCount());
    }

    @After
    public void deleteUser() throws Exception
    {
        DBServiceImpl DB = new DBServiceImpl();
        PreparedStatement state = DB.getConnection().prepareStatement("DELETE FROM UserProfile WHERE user_name=?;");
        state.setString(1,user.getLogin());
        state.execute();
    }
}