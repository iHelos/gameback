package auth;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

/**
 * Created by shamilmakhmutov on 04.12.15.
 */
public class SecurityMD5Test {

    private ArrayList<String> testStrings = new ArrayList<>();

    @Before
    public final void fillTestArray() {
        testStrings.add("vcvcvsadessdada");
        testStrings.add("sdghjasgdjhasd");
        testStrings.add("asadnkjasdnjkades");
        testStrings.add("asdkljmlkmklmkllmkdasdas");
        testStrings.add("sdkmlmlkmadsadsa");
        testStrings.add("adasdasdsad");
        testStrings.add("dsadasfsfdkjlvfsd");
        testStrings.add("dsadsadad3dsfsdfad");
        testStrings.add("dsad3asdassdadsadfa");
    }

    @After
    public final void cleanTestArray() {
        testStrings = null;
    }

    @Test
    public void md5Test() {
        for (String testString : testStrings) {
            assert (isMD5(SecurityMD5.getMD5(testString)));
            assertEquals(SecurityMD5.getMD5(testString), SecurityMD5.getMD5(testString));
        }
        assertEquals(SecurityMD5.getMD5("1"), "c4ca4238a0b923820dcc509a6f75849b");
        assertEquals(SecurityMD5.getMD5("Technopark"), "202cc2f161018a61a165a13173e1cbd2");
        assertEquals(SecurityMD5.getMD5("BMSTU"), "918bdd22d91a41aeecd5504837a9fad7");
        assertEquals(SecurityMD5.getMD5("Java"), "d52387880e1ea22817a72d3759213819");
        assertEquals(SecurityMD5.getMD5("Java Script"), "8a54555905db960c6d0a62b7418ad4ad");
    }

    private boolean isMD5(String data) {
        if (data == null) {
            return false;
        }
        final Pattern patternMD5 = Pattern.compile("^[a-f0-9]{32}$");
        Matcher matcherMD5 = patternMD5.matcher(data);
        return matcherMD5.matches();
    }

}
