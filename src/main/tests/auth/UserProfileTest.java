package auth;

import database.datasets.UserProfile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by olegermakov on 30.10.15.
 */
public class UserProfileTest {

    private UserProfile user;

    @Before
    public void setUp() throws Exception {
        user = new UserProfile("login", "password", "email");
    }

    @Test
    public void testGetLogin() throws Exception {
        assertEquals(user.getLogin(),"login");
    }

    @Test
    public void testGetPassword() throws Exception {
        assertEquals(user.getPasswordMD5(),"password");
    }

    @Test
    public void testGetEmail() throws Exception {
        assertEquals(user.getEmail(),"email");
    }

    @Test
    public void testGetWinCount() throws Exception {
        assertEquals(user.getWinCount(),0);
        UserProfile tempuser = new UserProfile("login", "password", "email",1,0,0);
        assertEquals(tempuser.getWinCount(), 1);
    }

    @Test
    public void testGetLoseCount() throws Exception {
        assertEquals(user.getLoseCount(),0);
        UserProfile tempuser = new UserProfile("login", "password", "email",0,1,0);
        assertEquals(tempuser.getLoseCount(), 1);
    }

    @Test
    public void testGetDrawCount() throws Exception {
        assertEquals(user.getDrawCount(),0);
        UserProfile tempuser = new UserProfile("login", "password", "email",0,0,1);
        assertEquals(tempuser.getDrawCount(), 1);
    }

    @Test
    public void testGetGameCount() throws Exception {
        assertEquals(user.getGameCount(), 0);
        UserProfile tempuser = new UserProfile("login", "password", "email",1,1,1);
        assertEquals(tempuser.getGameCount(), 3);
    }

    @Test
    public void testIncrementWinCount() throws Exception {
        assertEquals(user.getWinCount(), 0);
        user.incrementWinCount();
        assertEquals(user.getWinCount(), 1);
    }

    @Test
    public void testIncrementLoseCount() throws Exception {
        assertEquals(user.getLoseCount(),0);
        user.incrementLoseCount();
        assertEquals(user.getLoseCount(), 1);
    }

    @Test
     public void testCopy() throws Exception {
//        UserProfile temp = new UserProfile(user);
//        assertEquals(temp.getLogin(),user.getLogin());
//        assertEquals(temp.getPasswordMD5(),user.getPasswordMD5());
//        assertEquals(temp.getEmail(),user.getEmail());
    }


}