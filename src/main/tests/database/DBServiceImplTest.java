package database;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import resourceSystem.resourceObjects.ServerConfig;
import org.mockito.mock.*;

import javax.print.DocFlavor;
import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * Created by shamilmakhmutov on 07.12.15.
 */
public class DBServiceImplTest {
    private String DB_user = "root";
    private String DB_pass = "sadasd";
    private String DB_ip = "127.0.0.1";
    private String DB_port = "3306";
    private String DB_name = "Football_test";
    private int port = 8083;

    ServerConfig serverConfig = mock(ServerConfig.class);

    @Before
    public void init() {
        when(serverConfig.getDB_user()).thenReturn(DB_user);
        when(serverConfig.getDB_name()).thenReturn(DB_name);
        when(serverConfig.getDB_pass()).thenReturn(DB_pass);
        when(serverConfig.getDB_ip()).thenReturn(DB_ip);
        when(serverConfig.getDB_port()).thenReturn(DB_port);
        when(serverConfig.getPort()).thenReturn(port);
    }

    @Test
    public void bla() {
    }

}
