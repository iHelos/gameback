package database.datasets;


import org.junit.After;
import org.junit.Test;
import java.util.Random;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by shamilmakhmutov on 07.12.15.
 */
public class UserProfileTest {

    private static final String LOGIN = "login";
    private static final String PASS = "pass";
    private static final String EMAIL = "email";

    private UserProfile user = null;


    @After
    public void resetUser() {
        user = null;
    }

    @Test
    public void createNew() {
        user = new UserProfile(LOGIN, PASS, EMAIL);
        assertNotNull(user);
        assertEquals(LOGIN, user.getLogin());
        assertEquals(PASS, user.getPasswordMD5());
        assertEquals(EMAIL, user.getEmail());
        assertEquals(0, user.getWinCount());
        assertEquals(0, user.getLoseCount());
        assertEquals(0, user.getDrawCount());
    }

    @Test
    public void createNewWithStat() {
        user = new UserProfile(LOGIN, PASS, EMAIL, 1, 2, 3);
        assertNotNull(user);
        assertEquals(LOGIN, user.getLogin());
        assertEquals(PASS, user.getPasswordMD5());
        assertEquals(EMAIL, user.getEmail());
        assertEquals(1, user.getWinCount());
        assertEquals(2, user.getLoseCount());
        assertEquals(3, user.getDrawCount());
    }
    @Test
    public void createNewWithUser() {
        UserProfile testProfile = new UserProfile(LOGIN, PASS, EMAIL, 3, 4 ,5);
        user = spy(new UserProfile(testProfile));
        assertNotNull(user);
        assertEquals(LOGIN, user.getLogin());
        assertEquals(PASS, user.getPasswordMD5());
        assertEquals(EMAIL, user.getEmail());
        assertEquals(3, user.getWinCount());
        assertEquals(4, user.getLoseCount());
        assertEquals(5, user.getDrawCount());
    }

    @Test
    public void incTest() {
        user= new UserProfile(LOGIN, PASS, EMAIL);
        Random random = new Random();
        int randomInt = random.nextInt(5) + 5;
        for (int i = 0; i < randomInt; ++i) {
            assertEquals(i + 1, user.incrementWinCount());
            assertEquals(i + 1, user.incrementLoseCount());
            assertEquals(i + 1, user.incrementDrawfCount());
        }

    }


}
