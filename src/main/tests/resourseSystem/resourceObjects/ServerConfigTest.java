package resourseSystem.resourceObjects;

import static org.junit.Assert.*;
import org.junit.Test;
import resourceSystem.resourceObjects.ServerConfig;
import testUtil.Validate;

import java.io.IOException;

/**
 * Created by shamilmakhmutov on 06.12.15.
 */
public class ServerConfigTest {

    @Test
    final public void initTest() {
        try {
            ServerConfig serverConfig = ServerConfig.getInstance();
            assertNotNull(serverConfig);

            int port = serverConfig.getPort();
            assertNotNull(port);
            assertEquals(true, 1024 <= port);
            assertEquals(true, 49151 >= port);

            String DB_user = serverConfig.getDB_user();
            assertNotNull(DB_user);
            assertEquals(true, DB_user.length() > 0);

            String DB_pass = serverConfig.getDB_pass();
            assertNotNull(DB_pass);

            String DB_ip = serverConfig.getDB_ip();
            assertNotNull(DB_ip);
            assertEquals(true, Validate.ip(DB_ip));

            String DB_port = serverConfig.getDB_port();
            assertNotNull(DB_port);
            assertEquals(true, Validate.number(DB_port));
            int portDB = Integer.parseInt(DB_port);
            assertEquals(true, 1024 <= portDB);
            assertEquals(true, 49151 >= portDB);

            String DB_name = serverConfig.getDB_name();
            assertNotNull(DB_name);
            assertEquals(true, DB_name.length() > 0);

        } catch (IOException e) {
            e.printStackTrace();
            assertEquals(true, false);
        }
    }

}
