package frontend;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by olegermakov on 30.10.15.
 */
public class ProfileServletTest extends ServletTest {

    @Test
    public void testDoGet() throws Exception {
        assertEquals(request.getSession().getId(), "test_id");
    }
}