package frontend;

import auth.AccountServiceImpl;
import base.AccountService;
import base.DBService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import database.DBServiceImpl;
import org.junit.Before;
import org.mockito.invocation.InvocationOnMock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by olegermakov on 01.11.15.
 */
public class ServletTest {

    protected DBService DB;
    protected AccountService userBase;

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;

    @Before
    public void init() throws Exception {

        DB = new DBServiceImpl();
        userBase = new AccountServiceImpl(DB);


        userBase = mock(AccountService.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);

        BufferedReader reader = new BufferedReader(new StringReader(""));

        when(request.getSession()).thenReturn(session);
        when(request.getReader()).thenReturn(reader);
        when(session.getId()).thenReturn("test_id");

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        when(response.getWriter()).thenReturn(printWriter);
        when(response.toString()).thenAnswer((InvocationOnMock invocationOnMock) -> stringWriter.toString());

    }

    public void setRequest(String requestText) throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(requestText));
        when(request.getReader()).thenReturn(reader);
    }

    public JsonObject getResponseAnswer() {
        String answer = response.toString();
        return new Gson().fromJson(answer, JsonObject.class);
    }


}
