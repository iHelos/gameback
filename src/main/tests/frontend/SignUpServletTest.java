package frontend;

import com.google.gson.JsonObject;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Created by olegermakov on 30.10.15.
 */
public class SignUpServletTest extends ServletTest {

    private SignUpServlet serv;
    @Test
    public void testDoPostBad() throws Exception {
        JsonObject req = new JsonObject();

        setRequest(req.toString());

        serv = new SignUpServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Test
    public void testDoPostNotJson() throws Exception {
        String req = "hello";

        setRequest(req);

        serv = new SignUpServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Test
    public void testDoPostNoField() throws Exception {

        JsonObject req = new JsonObject();
        req.addProperty("name", "SomeUser");
        req.addProperty("password", "");

        setRequest(req.toString());

        serv = new SignUpServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_NO_CONTENT);
    }
}