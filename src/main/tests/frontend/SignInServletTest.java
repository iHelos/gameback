package frontend;

import com.google.gson.JsonObject;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Created by olegermakov on 30.10.15.
 */
public class SignInServletTest extends  ServletTest {

    private SignInServlet serv;


    @Test
    public void testDoPostBadRequest() throws Exception {

        JsonObject req = new JsonObject();

        setRequest(req.toString());

        serv = new SignInServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Test
    public void testDoPostNotJson() throws Exception {

        String req = "Hello, I'm Admin";

        setRequest(req);

        serv = new SignInServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Test
    public void testDoPostNoField() throws Exception {

        JsonObject req = new JsonObject();
        req.addProperty("name", "SomeUser");
        req.addProperty("password", "");

        setRequest(req.toString());

        serv = new SignInServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

    @Test
    public void testDoPostBadUser() throws Exception {

        JsonObject req = new JsonObject();
        req.addProperty("name", "SomeUser");
        req.addProperty("password", "SomePass");

        setRequest(req.toString());

        serv = new SignInServlet(userBase);
        serv.doPost(request, response);

        JsonObject answer = getResponseAnswer();
        assertEquals(answer.get("code").getAsInt(), 1);
        verify(response).setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

//   @Test
//    public void testDoPostRightUser() throws Exception {
//        UserProfile admin = new UserProfile("admin","123","123");
//        userBase.addUser("admin", admin);
//
//
//
//        JsonObject req = new JsonObject();
//        req.addProperty("name", "admin");
//        req.addProperty("password", "123");
//
//        setRequest(req.toString());
//
//        serv = new SignInServlet(userBase);
//        serv.doPost(request, response);
//
//        JsonObject answer = getResponseAnswer();
//        assertEquals(answer.get("code").getAsInt(), 0);
//        verify(response).setStatus(HttpServletResponse.SC_OK);
//    }
}