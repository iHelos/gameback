package gameplay;

import base.DBService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import database.DBServiceImpl;
import database.datasets.UserProfile;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.Before;
import org.junit.Test;
import websockets.GameSocket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by olegermakov on 30.10.15.
 */
public class GameSessionTest {

    GameSession GSTest;
    GameSocket testSocket;

    @Before
    public void init() throws Exception
    {
        GSTest = new GameSession("TestLobby",2,2, new DBServiceImpl());
        GameSession.setSessionProperties(1000,1000,5,10,100);
        testSocket = mock(GameSocket.class);
        when(testSocket.getPlayer()).thenReturn(new GamePlayer(new UserProfile("test", "test", "test")));

        Session testSession = mock(Session.class);
        RemoteEndpoint rpTest = mock(RemoteEndpoint.class);
        when(testSession.getRemote()).thenReturn(rpTest);
        when(testSocket.getSession()).thenReturn(testSession);
    }

    @Test
    public void testChangeTeam() throws Exception {

    }

    @Test
    public void testGetUserCount() throws Exception {
        GSTest.addUser(testSocket);
        GSTest.addUser(testSocket);
        assertEquals(2, GSTest.getUserCount());
        GSTest.addUser(testSocket);
        assertEquals(2, GSTest.getUserCount());
    }

    @Test
    public void testGetSessionTime() throws Exception {
        GSTest.start();
        Thread.sleep(100);
        assertTrue(100 < GSTest.getSessionTime());
    }

    @Test
    public void testStart() throws Exception {
        assertEquals(false, GSTest.getStarted());
        GSTest.start();
        assertEquals(true, GSTest.getStarted());
    }

    @Test
    public void testGetWinner() throws Exception {

    }

    @Test
    public void testGetMaxCount() throws Exception {
        assertEquals(2, GSTest.getMaxCount());
    }

    @Test
    public void testGetJson() throws Exception {
        JsonObject jsonGet = GSTest.getJson();
        assertEquals("TestLobby", jsonGet.get("name").getAsString());
        assertEquals(2, jsonGet.get("maxCount").getAsInt());
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals("TestLobby", GSTest.getName());
    }

    @Test
    public void testWorldInfo() throws Exception{
        JsonObject world = GSTest.worldInfo();
        JsonArray balls = world.get("balls").getAsJsonArray();
        assertEquals(1,balls.size());
        GSTest.addUser(testSocket);
        GSTest.addUser(testSocket);
        world = GSTest.worldInfo();
        balls = world.get("balls").getAsJsonArray();
        assertEquals(3, balls.size());
    }

//    @Test
//    public void testMakeStep() throws Exception{
//        GSTest.start();
//        GSTest.makeStep();
//        verify(GSTest, times(1)).worldInfo();
//        Thread.sleep(1000);
//        verify(GSTest, times(0)).worldInfo();
//
//    }

    @Test
    public void testRemoveUser() throws Exception{
        GSTest.addUser(testSocket);
        assertEquals(1, GSTest.getUserCount());
        GSTest.removeUser(testSocket);
        assertEquals(0, GSTest.getUserCount());
    }
}