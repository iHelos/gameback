package gameplay;

import com.google.gson.JsonObject;
import database.datasets.UserProfile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by olegermakov on 30.10.15.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({GamePlayer.class, GameSession.class})
public class GamePlayerTest {

    GamePlayer gp;
    @Before
    public void init() throws IOException
    {
       // GameSession sad = PowerMockito.mock(GameSession.class);
        PowerMockito.mockStatic(GameSession.class);
        Mockito.when(GameSession.getMaxSpeed()).thenReturn(2);
        Mockito.when(GameSession.getWidth()).thenReturn(40);
        Mockito.when(GameSession.getHeight()).thenReturn(40);
        UserProfile up = new UserProfile("testName", "testPass", "testEmail");
        gp = new GamePlayer(up);
    }

    @Test
    public void testGetUserScore() throws Exception {
        assertEquals(0,gp.getUserScore());
    }

    @Test
    public void testGetStatistics() throws Exception {

    }

    @Test
    public void testSetTeam() throws Exception {

    }

    @Test
    public void testGetTeam() throws Exception {

    }


    @Test
    public void testIncrementUserScore() throws Exception {
        gp.incrementUserScore();
        assertEquals(1,gp.getUserScore());
    }

    @Test
    public void testDecrementUserScore() throws Exception {
        gp.decrementUserScore();
        assertEquals(-1,gp.getUserScore());
    }

    @Test
    public void testRightVx() throws Exception {
        gp.rightVx();
        assertEquals(1,gp.getVx());
        gp.rightVx();
        gp.rightVx();
        assertEquals(2,gp.getVx());
    }

    @Test
    public void testLeftVx() throws Exception {
        gp.leftVx();
        assertEquals(-1,gp.getVx());
        gp.leftVx();
        gp.leftVx();
        assertEquals(-2,gp.getVx());
    }

    @Test
    public void testTopVy() throws Exception {
        gp.topVy();
        assertEquals(1,gp.getVy());
        gp.topVy();
        gp.topVy();
        assertEquals(2,gp.getVy());
    }

    @Test
    public void testBottomVy() throws Exception {
        gp.bottomVy();
        assertEquals(-1,gp.getVy());
        gp.bottomVy();
        gp.bottomVy();
        assertEquals(-2,gp.getVy());
    }

    @Test
    public void testUpdateCoordinates() throws Exception {
        int x = gp.getX();
        int y = gp.getY();
        gp.updateCoordinates();
        assertEquals(x,gp.getX());
        assertEquals(y,gp.getY());
        gp.bottomVy();
        gp.rightVx();
        gp.updateCoordinates();
        assertEquals(x+1,gp.getX());
        assertEquals(y-1,gp.getY());
    }

    @Test
    public void testToJson() throws Exception {
        gp.setVy(7);
        gp.setVx(6);
        gp.setX(4);
        gp.setY(5);
        JsonObject result = gp.toJson();
        assertEquals(4, result.get("x").getAsInt());
        assertEquals(5, result.get("y").getAsInt());
        assertEquals(6, result.get("vx").getAsInt());
        assertEquals(7, result.get("vy").getAsInt());
    }

    public void testChangeSpeed() throws Exception {

    }

    @Test
    public void testGetX() throws Exception {
        assertEquals(0,gp.getX());
    }

    @Test
    public void testGetY() throws Exception {
        assertEquals(0,gp.getY());
    }

    @Test
    public void testSetX() throws Exception {
        gp.setX(5);
        assertEquals(5,gp.getX());
    }

    @Test
    public void testSetY() throws Exception {
        gp.setY(5);
        assertEquals(5,gp.getY());
    }

    @Test
    public void testSetVx() throws Exception {
        gp.setVx(5);
        assertEquals(5,gp.getVx());
    }

    @Test
    public void testSetVy() throws Exception {
        gp.setVx(5);
        assertEquals(5,gp.getVx());
    }

    @Test
    public void testGetVx() throws Exception {
        assertEquals(0,gp.getVx());
    }

    @Test
    public void testGetVy() throws Exception {
        assertEquals(0,gp.getVx());
    }

    @Test
    public void testGetSetRadius() throws Exception {
        gp.setRadius(4);
        assertEquals(4,gp.getRadius());
    }
}