package gameplay;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by iHelo on 15.11.2015.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Ball.class, GameSession.class})
public class BallTest extends TestCase {

    Ball ball;

    @Before
    public void init() {
        ball = new Ball();
        PowerMockito.mockStatic(GameSession.class);
        Mockito.when(GameSession.getMaxSpeed()).thenReturn(2);
        Mockito.when(GameSession.getWidth()).thenReturn(40);
        Mockito.when(GameSession.getHeight()).thenReturn(40);
    }

    public void testToJson() throws Exception {
    }

    public void testGetX() throws Exception {
        assertEquals(0, ball.getX());
    }

    public void testGetY() throws Exception {
        assertEquals(0, ball.getY());
    }

    public void testGetSetVx() throws Exception {
        ball.setVx(1);
        assertEquals(1, ball.getVx());
        ball.setVx(5);
        assertEquals(GameSession.getMaxSpeed(), ball.getVx());
    }

    public void testGetSetVy() throws Exception {
        ball.setVx(1);
        assertEquals(1, ball.getVx());
        ball.setVy(5);
        assertEquals(GameSession.getMaxSpeed(), ball.getVy());
    }

    public void testUpdateCoordinates() throws Exception {
        ball.setVx(1);
        ball.setVy(2);
       // ball.updateCoordinates();
        assertEquals(1, ball.getX());
        assertEquals(2, ball.getY());

    }

    public void testGetSetRadius() throws Exception {
        int ballRadius = Ball.getRadius();
        ball.setRadius(5);
        assertEquals(5,ball.getRadius());
        Ball.setRadius(ballRadius);
    }

}